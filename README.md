# Ghidra Docker containers

This is a set of docker containers made for ghidra.

## ghidra-headless - Plain ghidra in a docker

Based on my own openjdk container and only contains ghidra. Since openjdk is headless, this container is also headless.
It comes with a volume for your project on `/project`.

### ... from a Dockerfile

```dockerfile
FROM florian0/ghidra-headless:latest

ENTRYPOINT ["/opt/support/analyzeHeadless", "/project/", "project", "-import", "/data/export.gzf", "-scriptPath", "/opt/script", "-postscript", "main.py"]
```

### ... from the commandline

```shell
docker run --rm -it -v $(pwd):/data florian0/ghidra-headless:latest /opt/support/analyzeHeadless /project project -import /data/export.gzf -scriptPath /opt/script -postscript main.py
```

### ... from docker-compose

```yaml
version: '3'
services:
  ghidra:
    image: florian0/ghidra-headless:latest
    volumes:
      - ./data:/data
    entrypoint: ["/opt/support/analyzeHeadless", "/project/", "project", "-import", "/data/export.gzf", "-scriptPath", "/opt/script", "-postscript", "main.py"]
```

## ghidra-bridge - Because Jython 2.7 is just not enough

Comes with [ghidra-bridge from justfoxing](https://pypi.org/project/ghidra-bridge/) and python 3.10 pre-installed. 

### ghidra-bridge:server-

Will start in server mode by default. Accepts ENV variables to specify connection parameters.

* `SERVER_HOST`: Host address to bind to. Use 0.0.0.0 to bind on everything, or the container name/ip.
* `SERVER_PORT`: Port to listen on. Default: `4768`.



## openjdk

The official one was deprecated. I don't understand why and the recommended alternatives seem odd to me, so I'll roll
my own for now. It is ubuntu based and just installs openjdk-headless from apt.
