import requests


def version_from_tag_name(tag_name: str):
    # "Ghidra_10.2.3_build"
    version = tag_name.split('_')[1]

    # If it is the first major release, append a 0
    # e.g. Ghidra_10.3_build
    if version.count('.') == 1:
        version += ".0"

    return version


def get_latest_version():
    response = requests.get('https://api.github.com/repos/NationalSecurityAgency/ghidra/releases/latest')
    if response.status_code != 200:
        raise Exception("Request failed")

    resp_data = response.json()

    version = version_from_tag_name(resp_data['tag_name'])

    assets = resp_data['assets']

    asset = next(filter(lambda a: all(x in a["browser_download_url"] for x in ["PUBLIC", ".zip"]),
                        assets))

    browser_download_url = asset['browser_download_url']

    return version, browser_download_url


def get_all_releases():
    response = requests.get('https://api.github.com/repos/NationalSecurityAgency/ghidra/releases')
    if response.status_code != 200:
        raise Exception("Request failed")

    resp_data = response.json()

    for release in resp_data:
        version = version_from_tag_name(release['tag_name'])

        assets = release['assets']

        asset = next(filter(lambda a: all(x in a["browser_download_url"] for x in ["PUBLIC", ".zip"]),
                            assets))

        browser_download_url = asset['browser_download_url']

        yield version, browser_download_url
