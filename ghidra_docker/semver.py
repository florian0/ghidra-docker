import itertools
from typing import List, Dict


def parse_semantic_version(version):
    major, minor, patch = map(int, version.split('.'))
    return major, minor, patch


def format_semantic_version(version, level):
    if level == 'major':
        return f'{version[0]}'
    elif level == 'minor':
        return f'{version[0]}.{version[1]}'
    elif level == 'patch':
        return f'{version[0]}.{version[1]}.{version[2]}'


def find_latest_version(versions, level):
    latest_versions = {}
    for version in versions:
        version_tuple = parse_semantic_version(version)
        if level == 'major':
            key = version_tuple[0]
        elif level == 'minor':
            key = version_tuple[:2]
        elif level == 'patch':
            key = version_tuple[:3]
        else:
            assert False
        latest_versions[key] = version

    return latest_versions.values()


def yield_tags(level, latest_versions):
    for version in latest_versions:
        yield version, format_semantic_version(parse_semantic_version(version), level)


def get_all_tags(versions):
    for level in ['patch', 'minor', 'major']:
        for version, tag in yield_tags(level, find_latest_version(versions, level)):
            yield version, tag


def group_tags(tuples) -> Dict[str, List[str]]:
    sorted_tags = sorted(tuples)

    return {version: [tag for _, tag in group] for version, group in
            itertools.groupby(sorted_tags, key=lambda x: x[0])}


def get_tags_from_versions(versions: List[str]):
    return group_tags(get_all_tags(versions))
