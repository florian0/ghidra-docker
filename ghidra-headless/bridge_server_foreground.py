import os
from ghidra_bridge_server import GhidraBridgeServer
from ghidra_bridge_port import DEFAULT_SERVER_PORT
from jfx_bridge import bridge

if __name__ == "__main__":
    # legacy version - run the server in the foreground, so we don't break people's expectations
    GhidraBridgeServer.run_server(
        server_host=os.getenv("SERVER_HOST") or bridge.DEFAULT_HOST,
        server_port=os.getenv("SERVER_PORT") or DEFAULT_SERVER_PORT,
        response_timeout=bridge.DEFAULT_RESPONSE_TIMEOUT, background=False)
