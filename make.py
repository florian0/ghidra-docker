from jinja2 import Template
from ghidra_docker import get_all_releases, get_tags_from_versions, parse_semantic_version


def min_version(release):
    major, minor, patch = parse_semantic_version(release[0])

    min_major = 10
    min_minor = 2
    min_patch = 3

    return ((min_major < major)
            or (min_major == major and min_minor < minor)
            or (min_major == major and min_minor == minor and min_patch <= patch))


def main():
    releases = [(version, url) for version, url in filter(min_version, get_all_releases())]
    tags = get_tags_from_versions(list(map(lambda x: x[0], releases)))

    # Open the template file and read its contents
    with open("gitlab-ci.yml.j2") as template_file:
        template_str = template_file.read()

    # Create a Template object and render it with variables
    template = Template(template_str)

    rendered_template = template.render(releases=releases, tags=tags)
    with open(".gitlab-ci.yml", "w") as output_file:
        output_file.write(rendered_template)


if __name__ == '__main__':
    main()
